//
//  ViewController.m
//  NetworkingDemo
//
//  Created by James Cash on 25-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "PeopleTableViewCell.h"

@interface ViewController () <UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *headerLabel;
@property (nonatomic,strong) NSArray *people;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Networking stuff

- (NSURL*)apiURL {
    return [NSURL URLWithString:@"https://swapi.co/api/people/?format=json"];
}

- (void)loadData {
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.apiURL];
//    [request addValue:@"foo" forHTTPHeaderField:@"hello"];
//    [request setHTTPMethod:@"POST"];
    NSURLSessionTask *task =
    [[NSURLSession sharedSession]
     dataTaskWithURL:[self apiURL]
     completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
         // All this stuff is happening on a background thread
         //         NSLog(@"Finished request: %@, %@, %@", response, error, data);
         if (error != nil) {
             NSLog(@"Error fetching from API: %@", error);
             abort();
         }
         if (((NSHTTPURLResponse*)response).statusCode != 200) {
             NSLog(@"Bad response from API server: %@", response);
             abort();
         }
         NSError *err = nil;
         // we just need to know from reading the api docs what this type will be
         NSDictionary *info = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
         if (err != nil) {
             NSLog(@"Error parsing JSON: %@", err.localizedDescription);
             abort();
         }
         //         NSLog(@"Parsed data: %@", info);
         NSMutableArray *infoPeople = [[NSMutableArray alloc] init];
         for (NSDictionary *person in info[@"results"]) {
             [infoPeople addObject:person[@"name"]];
         }
         // even though we're inside this "frozen" block of code that isn't being run until later, inside of NSURLSession things
         // we can still access outside variables like `self`
         self.people = [NSArray arrayWithArray:infoPeople];
         // ^^ in the background thread
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{
             // on the main thread
             // because UI stuff can only happen on the main thread
             [self.tableView reloadData];
             self.headerLabel.text = [NSString stringWithFormat:@"Showing %ld items", self.people.count];
         }];
     }];
    // Always remember to start the task after creating it!
    [task resume];
    NSLog(@"Created task");
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.people.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PeopleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PeopleCell"];

    [cell configureWithPerson:self.people[indexPath.row]];

    return cell;
}

@end
