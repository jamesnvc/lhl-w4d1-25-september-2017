//
//  PeopleTableViewCell.h
//  NetworkingDemo
//
//  Created by James Cash on 25-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeopleTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;


- (void)configureWithPerson:(NSString*)name;
@end
